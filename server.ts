/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/async/async.d.ts" />
/// <reference path="typings/express/express.d.ts" />
/// <reference path="typings/nconf/nconf.d.ts" />
/// <reference path="typings/body-parser/body-parser.d.ts" />
/// <reference path="typings/rethinkdbdash/rethinkdbdash.d.ts" />
/// <reference path="typings/lodash/lodash.d.ts" />
/// <reference path="typings/winston/winston.d.ts" />

import express = require('express');
import nconf = require('nconf');
import bodyParser = require('body-parser');
import rdash = require('rethinkdbdash');
import _ = require('lodash');
import log = require('winston');

import models = require('./models/index');
import routes = require('./routes/index');

// Nconf
nconf.argv()
    .env()
    .file({
        file: 'config/development.json'
    });

// Setup logging
log.level = nconf.get('logging:level') || 'debug';

// create app and DB connection
var app = express();
var r = rdash({
    db: nconf.get('rethinkdb:dbName')
});

// Setup app
app.set('nconf', nconf);
app.set('rethinkdb', r);

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Send error message
app.use(<express.ErrorRequestHandler> function(err, req, res, next) {
    res.status(500).json({
        status: 'error',
        message: err.message
    });
    next(err);
});


// Initialize routes
routes(app);

//models.createTables(r);

// A route
app.get('/', function (req, res) {
    res.send('hello');
});

// Server
var server = app.listen(nconf.get('port'), function () {
    var port = server.address().port;
    log.info('Listening on http://localhost:%s', port);
});

export = app;
