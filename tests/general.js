var request = require('supertest'),
    app = require('../server.js');

describe('GET /', function () {
    it('returns 200', function (done) {
        request(app)
            .get('/')
            .expect(200, done);
    });
});
