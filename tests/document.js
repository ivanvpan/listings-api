var request = require('supertest'),
    log = require('winston');
    app = require('../server.js');

log.level = 'error';

var testObjName = '==mocha test name==',
    id;

describe('POST /api/v1/document', function () {
    it('Saves objects', function (done) {
        request(app)
            .post('/api/v1/document')
            .send({
				name: testObjName,
				blueprintId: 'fake_id1',
				fieldValues: [{
					value: 'some_value',
					fieldId: 'some_id1',
				}]
            })
            .expect(200)
			.expect(function (res) {
                var data = res.body.data;
                if (data.inserted === 0) {
                    return new Error('Number of insertions returned is 0');
                }

                if (data.generated_keys.length < 1) {
                    return new Error('No generated keys returned');
                }

                id = data.generated_keys[0];
			})
			.end(done);
    });
});

describe('GET /api/v1/document', function () {
    it('responds with json', function (done) {
        request(app)
            .get('/api/v1/document')
            //.expect('Content-Type', /json/)
			.expect(200)
			.expect(function (res) {
                if (res.body.data.length < 1) {
                    return new Error('Did not get any documents');
                }
			})
			.end(done);
    });
});

describe('GET /api/v1/document/:id', function () {
    it('responds with 404 when id is invalid', function (done) {
        request(app)
            .get('/api/v1/document/randomness')
            .expect('Content-Type', /json/)
            .expect(404, done);
    });

    it('responds with a document', function (done) {
        request(app)
            .get('/api/v1/document/' + id)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                var data = res.body.data;
                if (!data) {
                    return new Error('Missing data');
                }
                if (!data.id) {
                    return new Error('Invalid object, no id');
                }
                if (data.name != testObjName) {
                    return new Error('Document name not what expected. Got: ' + data.name);
                }
            })
            .end(done);
    });
});

describe('PUT /api/v1/document/:id', function () {
    it('responds with 200', function (done) {
        request(app)
            .get('/api/v1/document/' + id)
            .end(function (err, res) {
                if (err) {
                    done(err);
                    return;
                }
                var obj = res.body.data;
                obj.name = 'newname';
                request(app)
                    .put('/api/v1/document/' + id)
                    .send(obj)
                    .expect(200, done);
            });
    });

    it('actually modifies resource', function (done) {
        setTimeout(function () {
            request(app)
                .get('/api/v1/document/' + id)
                .expect('content-type', /json/)
                .expect(200)
                .expect(function (res) {
                    if (!res.body.data) {
                        return new Error('missing data');
                    }
                    if (res.body.data.name != 'newname') {
                        return new Error('name has not been set');
                    }
                })
                .end(done);
        }, 1000);
    });
});

describe('DELETE /api/v1/document/:id', function () {
    it('Deletes test object', function (done) {
        request(app)
            .delete('/api/v1/document/' + id)
            .expect(200)
            .expect(function (res) {
                var data = res.body.data;
                if (!data.deleted == 1) {
                    return new Error('No objects deleted');
                }
            })
            .end(done);
    });
});
