/// <reference path="../typings/tsd.d.ts" />

import nconf = require('nconf');
import _ = require('lodash');
import log = require('winston');

export function createTables(r) {
    var dbName = nconf.get('rethinkdb:dbName'),
        tables = [
            'blueprint',
            'document',
            'field'
        ];
    r.dbCreate(dbName).run().then(function (result) {
        log.debug('db created', result);
    }).catch(function (err) {
        log.debug('db create err', err);
    });

    _.forEach(tables, function (table) {
        r.db(dbName).tableCreate(table).run().then(function (result) {
            log.debug('table', result);
        }).catch(function (err) {
            log.debug('table err', err);
        });
    });
}

/*
export enum FieldType {
    ShortText = 'shortText',
    SingleChoice = 'singleChoice'
}
*/


class Model {
    // List of properties that will be converted serialized/deserialized
    _properties: string[];
    toJSON(): any {
        var obj = {};
        _.forEach(this._properties, prop => obj[prop] = this[prop]);
        return obj;
    }
    deserialize(obj: any): any {
        _.forEach(this._properties, prop => this[prop] = obj[prop]);
    }
    constructor(obj?: any) {
        if (obj) {
            this.deserialize(obj);
        }
    }
}

export interface FieldType {
    name: string;
    humanMan: string;
    validate: (field: FieldModel, value: any) => string;
}

export var fieldTypes = {
    shortText: {
        name: 'shortText',
        humanName: 'Short text',
        validate: function (field: FieldModel, value: any): string {
            if (field.required && !value) {
                return 'Value may not be blank';
            }
        }
    },
    singleChoice: {
        name: 'singleChoice',
        humanName: 'Single choice',
        validate: function (field: FieldModel, value: any): string {
            if (field.required && !value) {
                return 'Value may not be blank';
            }
            if (_.indexOf(field.choices, value) < 0) {
                return 'Value "' + value + '" not one of allowed choices';
            };
        }
    }
};

export class BlueprintModel extends Model{
    static tableName: string = 'blueprint';
    _properties: string[];
    id: string;
    name: string;
    fieldIds: string[]; // id reference to field definitions (FieldModel)
    constructor(obj?: any) {
        this._properties = ['id', 'name', 'fieldIds'];
        super(obj);
    }
}

export class FieldModel extends Model {
    static tableName: string = 'field';
    _properties: string[];
    id: string;
    name: string;
    required: boolean;
    choices: string[];
    default: string;
    type: string; // one of fieldTypes
    constructor(obj?: any) {
        this._properties = ['id', 'name', 'required', 'choices', 'default', 'type'];
        super(obj);
    }
    // returns error string if error, null otherwise
    validate(): string {
        if (!this.name) {
            return 'Name field cannot be blank';
        }

        if (!fieldTypes[this.type]) {
            return 'Unrecognized field type ' + this.type;
        }
    }
}

export interface FieldValue {
    value: any;
    fieldId: string; // id reference to field definition  (FieldModel)
}

export class DocumentModel extends Model {
    static tableName: string = 'document';
    _properties: string[];
    id: string;
    name: string;
    blueprintId: string; // db ID of the blueprint object this doc implements
    fieldValues: FieldValue[];
    constructor(obj?: any) {
        this._properties = ['id', 'name', 'blueprintId', 'fieldValues'];
        super(obj);
    }
}

