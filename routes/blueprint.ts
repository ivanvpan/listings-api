import async = require('async');
import _ = require('lodash');
import express = require('express');
import rdash = require('rethinkdbdash');
import log = require('winston');


import models = require('../models/index');
import util =  require('./util');

export var init = function (app: express.Express) {
    var tableName = models.BlueprintModel.tableName,
        r: rdash.R = app.get('rethinkdb');

    function validateBlueprint(req, res, next) {
        var body = req.body;

        function sendError(err: string) {
            util.sendError(err, res, next, 400);
        }

        if (!body.name) {
            return sendError('Name field cannot be blank');
        }

        if (!_.isArray(body.fieldIds)) {
            return sendError('fieldsIds must be an array');
        }

        next();
        //TODO make sure that all the fieldIds are valid
        /*
        r.table(tableName).getAll(body.fieldIds).run().then(function (count) {
            next();
        })
        .catch(function(err) {
            next(err)
        });
        */
    };

    app.get('/api/v1/blueprint', function (req, res, next) {
        var promise = r.table(tableName).run();
        util.sendResult(promise, res, next);
    });

    app.get('/api/v1/blueprint/:id', function (req, res, next) {
        var promise = r.table(tableName).get(req.params.id).run();
        util.sendResultOr404(promise, res, next);
    });

    app.post('/api/v1/blueprint',
        util.middleware.stripId,
        validateBlueprint,
        function (req, res, next) {
            log.debug('creating blueprint from req.body', req.body);
            var bp = new models.BlueprintModel(req.body),
                promise;

            promise = r.table(tableName).insert(bp.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.put('/api/v1/blueprint/:id',
        util.middleware.stripId,
        validateBlueprint,
        function (req, res, next) {
            var bp = new models.BlueprintModel(req.body),
                promise;
            bp.id = req.params.id;

            promise = r.table(tableName).get(req.params.id).replace(bp.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.delete('/api/v1/blueprint/:id', function (req, res, next) {
        var promise = r.table(tableName).get(req.params.id).delete().run();
        util.sendResult(promise, res, next);
    });
};
