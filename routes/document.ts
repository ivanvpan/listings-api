import async = require('async');
import _ = require('lodash');
import express = require('express');
import rdash = require('rethinkdbdash');
import log = require('winston');

import models = require('../models/index');
import util =  require('./util');

export var init = function (app: express.Express) {
    var tableName = models.DocumentModel.tableName,
        r: rdash.R = app.get('rethinkdb');

    function validateDocument(req, res, next) {
        var body = req.body,
            fieldIds;

        function sendError(err: string) {
            util.sendError(err, res, next, 400);
        }

        if (!body.name) {
            return sendError('Name field cannot be blank');
        }

        if (!body.blueprintId) {
            return sendError('No blueprint ID!?');
        }

        fieldIds = _.map(this.fieldValues, (value: models.FieldValue) => {
            return value.fieldId;
        });

        r.table(tableName).getAll(fieldIds).run().then((result) => {
            var errors: string[] = [];
            _.each(result, (field) => {
                var value = _.find(body.fieldValues, (val: models.FieldValue) => {
                        val.fieldId === field.id;
                    }),
                    fieldType: models.FieldType = models.fieldTypes[field.type],
                    err: string = fieldType.validate(field, value);
                if (err) {
                    errors.push(err);
                }
            });

            if (errors.length) {
                sendError(errors[0]);
            } else {
                next();
            }
        })
        .catch((err) => {
            next(err);
        });
    }

    app.get('/api/v1/document', function (req, res, next) {
        var promise = r.table(tableName).run();
        util.sendResult(promise, res, next);
    });

    app.get('/api/v1/document/:id', function (req, res, next) {
        var promise = r.table(tableName).get(req.params.id).run();
        util.sendResultOr404(promise, res, next);
    });

    app.post('/api/v1/document',
        util.middleware.stripId,
        validateDocument,
        function (req, res, next) {
            log.debug('creating document from req.body', req.body);
            var doc = new models.DocumentModel(req.body),
                promise;

            promise = r.table(tableName).insert(doc.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.put('/api/v1/document/:id',
        util.middleware.stripId,
        validateDocument,
        function (req, res, next) {
            var doc = new models.DocumentModel(req.body),
                promise;
            doc.id = req.params.id

            promise = r.table(tableName).get(req.params.id).replace(doc.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.delete('/api/v1/document/:id', function (req, res, next) {
        var promise = r.table(tableName).get(req.params.id).delete().run();
        util.sendResult(promise, res, next);
    });
};
