/// <reference path="../typings/tsd.d.ts" />

import express = require('express');
import rdash = require('rethinkdbdash');

export module middleware {
    export function stripId(req: express.Request, res: express.Response, next: Function ) {
        if (req.body) {
            delete req.body.id;
        }
        next();
    }
}

export function sendError(err: string, res: express.Response, next: Function, status?: number) {
    if (status) {
        res.status(status).json({
            status: 'fail',
            message: err
        });
    } else {
        next(err);
    }
}

export function sendResult(promise: rdash.Promise<any>, res: express.Response, next: Function) {
    promise.then(function (result) {
        if (result.errors && result.errors > 0) {
            next(result.first_error);
        } else {
            res.json({
                status: 'success',
                data: result
            });
        }
    }).catch(function (err) {
        next(err);
    });
}

export function sendResultOr404(promise: rdash.Promise<any>, res: express.Response, next: Function) {
    promise.then(function (result) {
        if (!result) {
            res.status(404).json({
                status: 'fail',
                message: 'Not found'
            });
        } else {
            res.json({
                status: 'success',
                data: result
            });
        }
    }).catch(function (err) {
        next(err);
    });

}
