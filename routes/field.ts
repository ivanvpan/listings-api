/// <reference path="../typings/tsd.d.ts" />

import async = require('async');
import _ = require('lodash');
import express = require('express');
import rdash = require('rethinkdbdash');
import log = require('winston');

import models = require('../models/index');
import util =  require('./util');

export var init = function (app: express.Express) {
    var tableName = 'field',
        r: rdash.R = app.get('rethinkdb');

    function validateField(req, res, next) {
        var body = req.body;

        function sendError(err: string) {
            util.sendError(err, res, next, 400);
        }

        if (!body.name) {
            return sendError('Name field cannot be blank');
        }
        next();
    }

    app.get('/api/v1/field/fieldTypes', function (req, res, next) {
        res.json({
            status: 'success',
            data: models.fieldTypes
        });
    });

    app.get('/api/v1/field', function (req, res, next) {
        var promise = r.table(tableName).run();
        util.sendResult(promise, res, next);
    });

    app.get('/api/v1/field/:id', function (req, res, next) {
        var promise = r.table(tableName).get(req.params.id).run();
        util.sendResultOr404(promise, res, next);
    });

    app.post('/api/v1/field',
        util.middleware.stripId,
        validateField,
        function (req, res, next) {
            log.debug('creating field from req.body', req.body);
            var field = new models.FieldModel(req.body),
                promise;

            promise = r.table(tableName).insert(field.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.put('/api/v1/field/:id',
        util.middleware.stripId,
        validateField,
        function (req, res, next) {
            var field = new models.FieldModel(req.body),
                promise;
            field.id = req.params.id;

            promise = r.table(tableName).get(req.params.id).replace(field.toJSON()).run();
            util.sendResult(promise, res, next);
        });

    app.delete('/api/v1/field/:id', function (req, res, next) {
        var promise = r.table(tableName).delete(req.params.id).run();
        util.sendResult(promise, res, next);
    });
};
